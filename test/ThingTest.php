<?php

use \Example\models\Thing;

class ThingTest extends PHPUnit_Framework_TestCase
{
    public function test_saving_a_thing_creates_a_new_thing()
    {
        //Set-Up
        $db = $this->getMockBuilder('\Example\Database')
                           ->setMethods(array('save'))
                           ->getMock();

        //Our Database::save() method should be called only once
        //and should be provided with an instance of \Example\Thing
        $db->expects($this->once())
            ->method('save')
            ->will($this->returnCallback(function ($thing) {
                 PHPUnit_Framework_TestCase::assertInstanceOf('\Example\models\Thing', $thing);
                 return (int)1;
            }));

        $thing = new Thing($db);
        
        //Act
        $saved_thing_id = $thing->save($thing);

        //Assert
        PHPUnit_Framework_TestCase::assertEquals($saved_thing_id, 1);
    }
}
