<?php

namespace Example\models;

/**
 * Thing
 *
 * This class models a "Thing"
 */
class Thing
{

    /**
     * An instance of a database class
     * @var \Example\Database
     */
    private $db = null;

    /**
     * Constructor
     * @param \Example\Database $db An instance of a database manager
     */
    public function __construct(\Example\Database $db)
    {
        $this->db = $db;
    }

    /**
     * Save
     * @param  $data
     * @return void
     */
    public function save($data)
    {
        return $this->db->save($this);
    }
}
