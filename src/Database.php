<?php 

namespace Example;

/**
* Database
*
* Stub database class. This class exists solely for the purpose of mocking it out
* so that we can create test doubles based on it. This shouldn't be tested because
* in a real-world system it would be part of the ORM
*/
class Database
{
    /**
     * Save
     * @param  mixed $data Some stuff to save
     * @return integer
     */
    public function save($data)
    {
        return (int)1;
    }

    /**
     * This database class will always return an object of type \Example\models\Thing
     * @param  int $id The ID of the thing we wish to retrieve from the database
     * @return \Example\Thing
     */
    public function get($id)
    {
        return new \Example\models\Thing($this);
    }
}
